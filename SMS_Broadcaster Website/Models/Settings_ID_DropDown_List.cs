﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMS_Broadcaster_Website.Models
{
    public class Settings_ID_DropDown_List
    {
        public int settings_id { get; set; }
        public string hostname { get; set; }
        public List<SelectListItem> ServerList { get; set; }
    }
}