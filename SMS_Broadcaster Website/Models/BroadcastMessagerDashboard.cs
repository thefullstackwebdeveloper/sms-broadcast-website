﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS_Broadcaster_Website.Models
{
    public class BroadcastMessagerDashboard
    {
        public string Destination_Addr { get; set; }
        public int Count { get; set; }
    }
}