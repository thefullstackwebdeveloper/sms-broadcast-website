﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS_Broadcaster_Website.Models
{
    public class BroadcastMessagerDashboardSettings
    {
        public int Settings_ID { get; set; }
        public int Count { get; set; }
    }
}