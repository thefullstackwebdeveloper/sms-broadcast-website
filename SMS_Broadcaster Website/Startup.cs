﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SMS_Broadcaster_Website.Startup))]
namespace SMS_Broadcaster_Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
