﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMS_Broadcaster_Website.Models;
using SMS_Broadcaster_Website.Extensions;
using System.Data.Entity.Validation;
using System.Diagnostics;
using PagedList.EntityFramework;
using System.IO;

namespace SMS_Broadcaster_Website.Controllers
{
    [Authorize]
    public class Broadcast_MessagesController : Controller
    {

        private Forest_InteractiveEntities db = new Forest_InteractiveEntities();

        // GET: Dashboard
        public async Task<ActionResult> Dashboard(int? page)
        {
            Set_Up();
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var contact_group = (from messages in db.Broadcast_Messages select messages)
                                .OrderBy(message => message.Date_Time_Send)
                                .GroupBy(message => message.Destination_Addr)
                                .Select(group => new BroadcastMessagerDashboard
                                {
                                    Destination_Addr = group.Key,
                                    Count = group.Count()
                                }).Take(10);

            contact_group = contact_group.OrderBy(message => message.Destination_Addr);

            var setting_group_pending = (from messages in db.Broadcast_Messages select messages)
                                 .Where(message => message.Date_Time_Send > DateTime.Now)
                                 .GroupBy(message => message.Settings_ID)
                                 .Select(group => new BroadcastMessagerDashboardSettings
                                 {
                                     Settings_ID = (int)group.Key,
                                     Count = group.Count()
                                 })
                                 .Take(10)
                                 .ToArray();

            var setting_group_accepted = (from messages in db.Broadcast_Messages select messages)
                               .Where(message => message.Sent == "true")
                               .GroupBy(message => message.Settings_ID)
                               .Select(group => new BroadcastMessagerDashboardSettings
                               {
                                   Settings_ID = (int)group.Key,
                                   Count = group.Count()
                               })
                               .Take(10)
                               .ToArray();

            var setting_group_rejected = (from messages in db.Broadcast_Messages select messages)
                                .Where(message => message.Sent != "true" && (message.Date_Time_Send < DateTime.Now))
                                .GroupBy(message => message.Settings_ID)
                                .Select(group => new BroadcastMessagerDashboardSettings
                                {
                                    Settings_ID = (int)group.Key,
                                    Count = group.Count()
                                })
                                .Take(10)
                                .ToArray();

            ViewData["setting_group_accepted"] = setting_group_accepted.ToList();
            ViewData["setting_group_rejected"] = setting_group_rejected.ToList();
            ViewData["setting_group_pending"] = setting_group_pending.ToList();

            return View(await contact_group.ToPagedListAsync(pageNumber, pageSize));

        }

        public async Task<ActionResult> Full_Statistics(int? page, string startDate, string endDate, string type = "")
        {
            
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            DateTime start = Convert.ToDateTime(startDate);
            DateTime end = Convert.ToDateTime(endDate);

            var contact_group_temp = from messages in db.Broadcast_Messages select messages;

            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                contact_group_temp = contact_group_temp.Where(m => m.Date_Time_Send >= start)
                                                       .Where(m => m.Date_Time_Send <= end);
            }

            contact_group_temp = contact_group_temp.OrderBy(message => message.Date_Time_Send);

            var contact_group = contact_group_temp
                                 .GroupBy(message => message.Destination_Addr)
                                 .Select(group => new BroadcastMessagerDashboard
                                 {
                                     Destination_Addr = group.Key,
                                     Count = group.Count(),
                                 });

            contact_group = contact_group.OrderBy(message => message.Destination_Addr);

            return View(await contact_group.ToPagedListAsync(pageNumber, pageSize));
        }

        public async Task<ActionResult> Accepted_Statistics(int? page, string startDate, string endDate, string type = "")
        {
            
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            DateTime start = Convert.ToDateTime(startDate);
            DateTime end = Convert.ToDateTime(endDate);

            var list_accepted_temp = from messages in db.Broadcast_Messages select messages;

            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                list_accepted_temp = list_accepted_temp.Where(m => m.Date_Time_Send >= start)
                                                       .Where(m => m.Date_Time_Send <= end);
            }

            var list_accepted = list_accepted_temp
                                .Where(message => message.Sent == "true")
                                .GroupBy(message => message.Destination_Addr)
                                .Select(group => new BroadcastMessagerDashboard
                                {
                                   Destination_Addr = group.Key,
                                   Count = group.Count()
                                });

            list_accepted = list_accepted.OrderBy(message => message.Destination_Addr);

            return View(await list_accepted.ToPagedListAsync(pageNumber, pageSize));
        }

        public async Task<ActionResult> Pending_Statistics(int? page, string startDate, string endDate, string type = "")
        {
            
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            DateTime start = Convert.ToDateTime(startDate);
            DateTime end = Convert.ToDateTime(endDate);

            var list_pending_temp = from messages in db.Broadcast_Messages select messages;

            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                list_pending_temp = list_pending_temp.Where(m => m.Date_Time_Send >= start)
                                                     .Where(m => m.Date_Time_Send <= end);
            }

            var list_pending = list_pending_temp
                               .Where(message => message.Date_Time_Send > DateTime.Now)
                               .GroupBy(message => message.Destination_Addr)
                               .Select(group => new BroadcastMessagerDashboard
                               {
                                  Destination_Addr = group.Key,
                                  Count = group.Count()
                               });

            /*var list_pending = (from messages in db.Broadcast_Messages select messages)
                                .Where(message => message.Date_Time_Send > DateTime.Now)
                                .GroupBy(message => message.Destination_Addr)
                                .Select(group => new BroadcastMessagerDashboard
                                {
                                    Destination_Addr = group.Key,
                                    Count = group.Count()
                                });
            */

            list_pending = list_pending.OrderBy(message => message.Destination_Addr);

            return View(await list_pending.ToPagedListAsync(pageNumber, pageSize));
        }

        public async Task<ActionResult> Rejected_Statistics(int? page, string startDate, string endDate, string type = "")
        {
            
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            DateTime start = Convert.ToDateTime(startDate);
            DateTime end = Convert.ToDateTime(endDate);

            var list_rejected_temp = from messages in db.Broadcast_Messages select messages;

            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                list_rejected_temp = list_rejected_temp.Where(m => m.Date_Time_Send >= start)
                                                       .Where(m => m.Date_Time_Send <= end);
            }

            var list_rejected = list_rejected_temp.Where(message => message.Sent != "true"
                                                            && (message.Date_Time_Send < DateTime.Now))
                                                  .GroupBy(message => message.Destination_Addr)
                                                  .Select(group => new BroadcastMessagerDashboard
                                                  {
                                                    Destination_Addr = group.Key,
                                                    Count = group.Count()
                                                  });
            /*
            var list_rejected = (from messages in db.Broadcast_Messages select messages)
                   .Where(message => message.Sent != "true"
                           && (message.Date_Time_Send < DateTime.Now)
                   )
                   .GroupBy(message => message.Destination_Addr)
                   .Select(group => new BroadcastMessagerDashboard
                   {
                       Destination_Addr = group.Key,
                       Count = group.Count()
                   });

            */

            list_rejected = list_rejected.OrderBy(message => message.Destination_Addr);

            return View(await list_rejected.ToPagedListAsync(pageNumber, pageSize));
        }

        // GET: Broadcast_Messages
        public async Task<ActionResult> Index(string sortOrder, int? page, string searchString, string startDate, string endDate)
        {
            
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            ViewBag.CurrentSort = sortOrder;
            ViewBag.DestinationSortParam = String.IsNullOrEmpty(sortOrder) ? "destination_desc" : "";
            ViewBag.DateSortParam = String.IsNullOrEmpty(sortOrder) ? "earliest" : "";

            var messages = from the_messages in db.Broadcast_Messages
                           select the_messages;

            DateTime start = Convert.ToDateTime(startDate);
            DateTime end = Convert.ToDateTime(endDate);

            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(m => m.Destination_Addr.Contains(searchString)
                                           || m.Message_ID.Contains(searchString)
                                          );
            }

            if (!String.IsNullOrEmpty(startDate) && !String.IsNullOrEmpty(endDate))
            {
                messages = messages.Where(m => m.Date_Time_Send >= start)
                                   .Where(m => m.Date_Time_Send <= end);
            }

            switch (sortOrder)
            {
                case "destination_desc":
                    messages = messages.OrderBy(the_messages => the_messages.Destination_Addr);
                    break;

                case "earliest":
                    messages = messages.OrderBy(the_messages => the_messages.Date_Time_Send);
                    break;

                default:
                    messages = messages.OrderByDescending(the_messages => the_messages.Date_Time_Send);
                    break;
            }


            return View(await messages.ToPagedListAsync(pageNumber, pageSize));
        }

        // GET: Broadcast_Messages/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Broadcast_Messages broadcast_Messages = await db.Broadcast_Messages.FindAsync(id);
            if (broadcast_Messages == null)
            {
                return HttpNotFound();
            }
            return View(broadcast_Messages);
        }

        // GET: Broadcast_Messages/Create
        public ActionResult Create()
        {
            
            var Settings_ID = from settings in db.SMPP_Settings select new { settings.Settings_ID, settings.Hostname };

            Settings_ID_DropDown_List model = new Settings_ID_DropDown_List();

            model.ServerList = Settings_ID
                               .Select(settings => new SelectListItem
                               {
                                   Text = settings.Settings_ID.ToString(),
                                   Value = settings.Settings_ID.ToString()
                               }).ToList();

            ViewData["List_Settings_ID"] = model.ServerList;
            return View();
        }

        // POST: Broadcast_Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Source_Addr,Destination_Addr,Service_Type,Data_Type,Message_ID,Text,Submit_Mode,Date_Time_Send,Date_Time_Created,Date_Time_Edited,Sent,Submitted,Settings_ID")] Broadcast_Messages broadcast_Messages, HttpPostedFileBase contact_file)
        {
            
            int contact_count = 0;
            try
            {

                if (ModelState.IsValid)
                {
                    List<String> contacts = contact_parser(contact_file);
                    contact_count = contacts.Count();

                    foreach (string contact in contacts)
                    {
                        Broadcast_Messages message = new Broadcast_Messages(); ;
                        message = broadcast_Messages;
                        message.Destination_Addr = contact;
                        Debug.Write(contact + "\n");
                        db.Broadcast_Messages.Add(message);
                        await db.SaveChangesAsync();
                    }
                    this.AddNotification("Successfully added " + contact_count + " messages", NotificationType.SUCCESS);
                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            this.AddNotification("An error has occurred", NotificationType.ERROR);
            return View(broadcast_Messages);
        }

        // GET: Broadcast_Messages/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Broadcast_Messages broadcast_Messages = await db.Broadcast_Messages.FindAsync(id);
            if (broadcast_Messages == null)
            {
                return HttpNotFound();
            }
            return View(broadcast_Messages);
        }

        // POST: Broadcast_Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Source_Addr,Destination_Addr,Service_Type,Data_Type,Message_ID,Text,Submit_Mode,Date_Time_Send,Date_Time_Created,Date_Time_Edited,Sent,Submitted,Settings_ID")] Broadcast_Messages broadcast_Messages)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    broadcast_Messages.Text.TrimEnd();
                    broadcast_Messages.Sent.Trim();
                    db.Entry(broadcast_Messages).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    this.AddNotification("Changes saved", NotificationType.SUCCESS);
                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            this.AddNotification("An error occurred", NotificationType.ERROR);
            return View(broadcast_Messages);
        }

        // GET: Broadcast_Messages/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Broadcast_Messages broadcast_Messages = await db.Broadcast_Messages.FindAsync(id);
            if (broadcast_Messages == null)
            {
                return HttpNotFound();
            }
            return View(broadcast_Messages);
        }

        // POST: Broadcast_Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Broadcast_Messages broadcast_Messages = await db.Broadcast_Messages.FindAsync(id);
            db.Broadcast_Messages.Remove(broadcast_Messages);
            await db.SaveChangesAsync();
            this.AddNotification("Message " + id + " deleted", NotificationType.SUCCESS);
            return RedirectToAction("Index");
        }


        /* Utility functions */
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected List<string> contact_parser(HttpPostedFileBase file)
        {
            List<string> contacts = new List<string>();
            string file_content = "";
            //if (file.ContentType == "text/plain")
            //{
            StreamReader reader = new StreamReader(file.InputStream);
            file_content = reader.ReadToEnd();
            //}
            //else
            //{
            //  file_content = "0";
            //}
            char delimiter = '\n';
            string[] temp = file_content.Split(delimiter)
                                        .Select(x => x.Trim())
                                        .Where(x => !string.IsNullOrWhiteSpace(x))
                                        .ToArray();
            contacts.AddRange(temp);
            return contacts;
        }

        public void Set_Up()
        {
            var list_pending = (from messages in db.Broadcast_Messages select messages)
                                .Where(message => message.Date_Time_Send > DateTime.Now)
                                .GroupBy(message => message.Destination_Addr)
                                .Select(group => new BroadcastMessagerDashboard
                                {
                                    Destination_Addr = group.Key,
                                    Count = group.Count()
                                })
                                .Take(10)
                                .ToArray();

            var list_accepted = (from messages in db.Broadcast_Messages select messages)
                                .Where(message => message.Sent == "true")
                                .GroupBy(message => message.Destination_Addr)
                                .Select(group => new BroadcastMessagerDashboard
                                {
                                    Destination_Addr = group.Key,
                                    Count = group.Count()
                                })
                                .Take(10)
                                .ToArray();

            var list_rejected = (from messages in db.Broadcast_Messages select messages)
                               .Where(message => message.Sent != "true"
                                       && (message.Date_Time_Send < DateTime.Now)
                               )
                               .GroupBy(message => message.Destination_Addr)
                               .Select(group => new BroadcastMessagerDashboard
                               {
                                   Destination_Addr = group.Key,
                                   Count = group.Count()
                               })
                               .Take(10)
                               .ToArray();

            ViewData["list_pending"] = list_pending.ToList();
            ViewData["list_accepted"] = list_accepted.ToList();
            ViewData["list_rejected"] = list_rejected.ToList();

            ViewData["authenticated"] = User.Identity.IsAuthenticated;
        }

    }
}
