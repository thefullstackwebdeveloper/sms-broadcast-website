﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMS_Broadcaster_Website.Models;
using SMS_Broadcaster_Website.Controllers;

namespace SMS_Broadcaster_Website
{
    [Authorize]
    public class SMPP_SettingsController : Controller
    {
        private Forest_InteractiveEntities db = new Forest_InteractiveEntities();
        private Broadcast_MessagesController the_controller = new Broadcast_MessagesController();

        // GET: SMPP_Settings
        public async Task<ActionResult> Index()
        {
 
            return View(await db.SMPP_Settings.ToListAsync());
        }

        // GET: SMPP_Settings/Details/5
        public async Task<ActionResult> Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMPP_Settings SMPP_Settings = await db.SMPP_Settings.FindAsync(id);
            if (SMPP_Settings == null)
            {
                return HttpNotFound();
            }
            return View(SMPP_Settings);
        }

        // GET: SMPP_Settings/Create
        public ActionResult Create()
        {
 
            return View();
        }

        // POST: SMPP_Settings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Settings_ID,System_id,Password,System_type,Hostname,Port,Source_Address_TON,Source_Address_NPI,Destination_Address_TON,Destination_Address_NPI")] SMPP_Settings sMPP_Settings)
        {
            if (ModelState.IsValid)
            {
                db.SMPP_Settings.Add(sMPP_Settings);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sMPP_Settings);
        }

        // GET: SMPP_Settings/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
 
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMPP_Settings sMPP_Settings = await db.SMPP_Settings.FindAsync(id);
            if (sMPP_Settings == null)
            {
                return HttpNotFound();
            }
            return View(sMPP_Settings);
        }

        // POST: SMPP_Settings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Settings_ID,System_id,Password,System_type,Hostname,Port,Source_Address_TON,Source_Address_NPI,Destination_Address_TON,Destination_Address_NPI")] SMPP_Settings sMPP_Settings)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sMPP_Settings).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sMPP_Settings);
        }

        // GET: SMPP_Settings/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
 
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMPP_Settings sMPP_Settings = await db.SMPP_Settings.FindAsync(id);
            if (sMPP_Settings == null)
            {
                return HttpNotFound();
            }
            return View(sMPP_Settings);
        }

        // POST: SMPP_Settings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
 
            SMPP_Settings sMPP_Settings = await db.SMPP_Settings.FindAsync(id);
            db.SMPP_Settings.Remove(sMPP_Settings);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
